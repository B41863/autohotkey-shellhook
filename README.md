# Shell Hook


## Synopsis

A library for AutoHotkey to attach a Shell Hook to the Windows API to listen for Shell events.

Refer to the [MSDN for RegisterShellHookWindow function](https://msdn.microsoft.com/en-us/library/windows/desktop/ms644989(v=vs.85).aspx) for more details.

## Usage

Download the source code and save it to a file, then include it in a script using AutoHotkey's [#Include](https://autohotkey.com/docs/commands/_Include.htm).

Extend the ShellHook class and Override the **ShellProc(hookCode, id)** method to implement actions to perform when a Shell event occurs.
